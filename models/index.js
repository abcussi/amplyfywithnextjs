// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Blog, Post, User } = initSchema(schema);

export {
  Blog,
  Post,
  User
};