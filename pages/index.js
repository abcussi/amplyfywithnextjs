import Head from "next/head";
import Image from "next/image";
import { useEffect, useState } from "react";
import styles from "../styles/Home.module.css";
import Link from "next/link";
import { DataStore } from "@aws-amplify/datastore";
import { Post } from "../models";

export default function Home() {
  const [posts, setPost] = useState([]);
  useEffect(() => {
    fetchPost();
    async function fetchPost() {
      const postData = await DataStore.query(Post);
      console.log(postData);
      setPost(postData);
    }
  }, []);
  return (
    <>
      <h1>Prueba Amplify</h1>
      {posts.map((post) => (
        <div>
          <Link href={`/post/${post.id}`}>
            <p>{console.log(post)}</p>
          </Link>
        </div>
      ))}
    </>
  );
}
