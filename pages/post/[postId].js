import React, { useEffect } from "react";

import { useRouter } from "next/router";

export const IndexPage = () => {
  const router = useRouter();
  const existInvoice = Object.keys(router.query).length !== 0;

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    console.log(router.query.postId);
  };
  return (
    <>
      <h1>Prueba de pagina query</h1>
      <h3>{router.query.postId}</h3>
    </>
  );
};

export default IndexPage;
